import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:stacked/stacked.dart';
import 'package:tiktok/data/video.dart';
import 'package:tiktok/screens/profile_screen.dart';
import 'package:tiktok/widgets/action_toolbar.dart';
import 'package:tiktok/widgets/video_description.dart';
import 'package:video_player/video_player.dart';

import '../widgets/bottom_bar.dart';
import 'chat_screen.dart';
import 'feed_viewmodel.dart';
import 'search_screen.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  final locator = GetIt.instance;
  final feedViewModel = GetIt.instance<FeedViewModel>();

  late TabController tabController;
  @override
  void initState() {
    feedViewModel.loadVideo(0);
    feedViewModel.loadVideo(1);
    tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<FeedViewModel>.reactive(
        disposeViewModel: false,
        builder: (context, model, child) => videoScreen(),
        viewModelBuilder: () => feedViewModel);
  }

  Widget videoScreen() {
    return Scaffold(
      backgroundColor: GetIt.instance<FeedViewModel>().actualScreen == 0
          ? Colors.black
          : Colors.white,
      body: Stack(
        children: [
          PageView.builder(
            itemCount: 2,
            onPageChanged: (value) {
              print(value);
              if (value == 1)
                SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
              else
                SystemChrome.setSystemUIOverlayStyle(
                    SystemUiOverlayStyle.light);
            },
            itemBuilder: (context, index) {
              if (index == 0)
                return scrollFeed();
              else
                return profileView();
            },
          )
        ],
      ),
    );
  }

  Widget scrollFeed() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(child: currentScreen()),
        BottomBar(),
      ],
    );
  }

  Widget currentScreen() {
    switch (feedViewModel.actualScreen) {
      case 0:
        return feedVideoes();
      case 1:
        return SearchScreen();
      case 2:
        return ChatScreen();
      case 3:
        return ProfileScreen();
      default:
        return feedVideoes();
    }
  }

  Widget feedVideoes() {
    return Stack(
      children: [
        PageView.builder(
          controller: PageController(
            initialPage: 0,
            viewportFraction: 1,
          ),
          itemCount: feedViewModel.videoSource?.listVideos.length,
          onPageChanged: (index) {
            index = index % (feedViewModel.videoSource!.listVideos.length);
            feedViewModel.changeVideo(index);
          },
          scrollDirection: Axis.vertical,
          itemBuilder: (context, index) {
            index = index % (feedViewModel.videoSource!.listVideos.length);
            return videoCard(feedViewModel.videoSource!.listVideos[index]);
          },
        ),
        SafeArea(
            child: Container(
          padding: EdgeInsets.only(top: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Following",
                style: TextStyle(
                    fontSize: 17.0,
                    fontWeight: FontWeight.normal,
                    color: Colors.white70),
              ),
              SizedBox(
                width: 7,
              ),
              Container(
                color: Colors.white70,
                height: 10,
                width: 1.0,
              ),
              SizedBox(
                width: 7,
              ),
              Text(
                'For You',
                style: TextStyle(
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              )
            ],
          ),
        )),
      ],
    );
  }

  Widget videoCard(Video video) {
    return Stack(
      children: [
        video.controller != null
            ? GestureDetector(
                onTap: () {
                  if (video.controller!.value.isPlaying)
                    video.controller?.pause();
                  else
                    video.controller?.play();
                },
                child: SizedBox.expand(
                  child: FittedBox(
                    fit: BoxFit.cover,
                    child: SizedBox(
                      width: video.controller?.value.size.width ?? 0,
                      height: video.controller?.value.size.height ?? 0,
                      child: VideoPlayer(video.controller!),
                    ),
                  ),
                ))
            : Center(
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(
                        color: Colors.white,
                        strokeWidth: 2.5,
                      ),
                      SizedBox(width: 15),
                      Text(
                        "Loading",
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                VideoDescription(video.user, video.videoTitle, video.songName),
                ActionsToolbar(video.likes, video.comments, video.userPic),
              ],
            ),
            SizedBox(height: 20),
          ],
        ),
      ],
    );
  }

  Widget profileView() {
    List<String> _images = [
      "https://media.giphy.com/media/tOueglJrk5rS8/giphy.gif",
      "https://media.giphy.com/media/665IPY24jyWFa/giphy.gif",
      "https://media.giphy.com/media/chjX2ypYJKkr6/giphy.gif",
      "https://media.giphy.com/media/sC60eX0OVIH7O/giphy.gif",
      "https://media.giphy.com/media/NsXhybxnMKsh2/giphy.gif",
      "https://media.giphy.com/media/HE6hyf47yAX1S/giphy.gif",
    ];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
        ),
        title: Text(
          "Charlotte Stone",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        actions: [
          Icon(
            Icons.more_horiz,
            color: Colors.black,
          ),
        ],
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 15),
            ClipOval(
              child: CachedNetworkImage(
                imageUrl:
                    "https://www.andersonsobelcosmetic.com/wp-content/uploads/2018/09/chin-implant-vs-fillers-best-for-improving-profile-bellevue-washington-chin-surgery.jpg",
                height: 120.0,
                width: 120.0,
                placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
            SizedBox(height: 10),
            Text(
              "@Charlotte21",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Text(
                      "412",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Following",
                      style: TextStyle(
                          fontSize: 12, fontWeight: FontWeight.normal),
                    ),
                  ],
                ),
                Container(
                  color: Colors.black54,
                  width: 1,
                  height: 15,
                  margin: EdgeInsets.symmetric(horizontal: 15),
                ),
                Column(
                  children: [
                    Text(
                      "1.1K",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Followers",
                      style: TextStyle(
                          fontSize: 12, fontWeight: FontWeight.normal),
                    ),
                  ],
                ),
                Container(
                  color: Colors.black54,
                  width: 1,
                  height: 15,
                  margin: EdgeInsets.symmetric(horizontal: 15),
                ),
                Column(
                  children: [
                    Text(
                      "17K",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Likes",
                      style: TextStyle(
                          fontSize: 12, fontWeight: FontWeight.normal),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 140,
                  height: 47,
                  decoration: BoxDecoration(color: Colors.pink[500]),
                  child: Center(
                    child: Text(
                      "Follow",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(width: 5),
                Container(
                  width: 45,
                  height: 47,
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.black12)),
                  child: Center(child: Icon(Icons.camera_alt)),
                ),
                SizedBox(width: 5),
                Container(
                  width: 35,
                  height: 47,
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.black12)),
                  child: Center(child: Icon(Icons.arrow_drop_down)),
                ),
              ],
            ),
            SizedBox(height: 15),
            Divider(
              color: Colors.black38,
            ),
            TabBar(
              controller: tabController,
              tabs: [
                Tab(
                  icon: Icon(
                    Icons.menu,
                  ),
                ),
                Tab(
                  icon: Icon(
                    Icons.favorite_outline_rounded,
                  ),
                ),
              ],
              unselectedLabelColor: Colors.grey.withOpacity(0.7),
              labelColor: Colors.black,
              indicatorColor: Colors.black87,
              indicatorSize: TabBarIndicatorSize.tab,
            ),
            Expanded(
              child: TabBarView(
                controller: tabController,
                children: [
                  GridView.builder(
                    shrinkWrap: true,
                    itemCount: _images.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 2,
                      mainAxisSpacing: 2,
                      childAspectRatio: 0.9,
                    ),
                    itemBuilder: (context, index) {
                      return Container(
                        height: 160,
                        decoration: BoxDecoration(
                            color: Colors.black26,
                            border:
                                Border.all(color: Colors.white70, width: .5)),
                        child: FittedBox(
                          child: CachedNetworkImage(
                            fit: BoxFit.fill,
                            imageUrl: _images[index],
                            placeholder: (context, url) => Padding(
                              padding: const EdgeInsets.all(35.0),
                              child: CircularProgressIndicator(),
                            ),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                          ),
                          fit: BoxFit.fill,
                        ),
                      );
                    },
                  ),
                  Center(
                    child: Text("You will find liked posts here",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
