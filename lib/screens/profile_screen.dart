import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen>
    with SingleTickerProviderStateMixin {
  List<String> _images = [
    "https://media.giphy.com/media/Ii4Cv63yG9iYawDtKC/giphy.gif",
    "https://media.giphy.com/media/tqfS3mgQU28ko/giphy.gif",
    "https://media.giphy.com/media/3o72EX5QZ9N9d51dqo/giphy.gif",
    "https://media.giphy.com/media/4oMoIbIQrvCjm/giphy.gif",
    "https://media.giphy.com/media/aZmD30dCFaPXG/giphy.gif",
    "https://media.giphy.com/media/NU8tcjnPaODTy/giphy.gif",
  ];

  late TabController tabController;
  @override
  void initState() {
    tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        leading: Icon(Icons.person_add_alt_1_outlined, color: Colors.black),
        title: Text(
          "Salvador Valverde",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.more_horiz,
              color: Colors.black,
            ),
          )
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 15),
          ClipOval(
            child: CachedNetworkImage(
              imageUrl:
                  "https://q5n8c8q9.rocketcdn.me/wp-content/uploads/2018/08/The-20-Best-Royalty-Free-Music-Sites-in-2018.png",
              height: 120.0,
              width: 120.0,
              fit: BoxFit.cover,
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          SizedBox(height: 10),
          Text(
            "@salvadordev",
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Text(
                    "54",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Following",
                    style:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.normal),
                  ),
                ],
              ),
              Container(
                color: Colors.black54,
                width: 1,
                height: 15,
                margin: EdgeInsets.symmetric(horizontal: 15),
              ),
              Column(
                children: [
                  Text(
                    "20",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Followers",
                    style:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.normal),
                  ),
                ],
              ),
              Container(
                color: Colors.black54,
                width: 1,
                height: 15,
                margin: EdgeInsets.symmetric(horizontal: 15),
              ),
              Column(
                children: [
                  Text(
                    "100",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Likes",
                    style:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.normal),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 140,
                height: 47,
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.black12)),
                child: Center(
                  child: Text(
                    "Edit Profile",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(width: 5),
              Container(
                width: 45,
                height: 47,
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.black12)),
                child: Center(
                  child: Icon(Icons.bookmark, color: Colors.black),
                ),
              ),
            ],
          ),
          SizedBox(height: 15),
          Divider(
            color: Colors.black38,
          ),
          TabBar(
            controller: tabController,
            tabs: [
              Tab(
                icon: Icon(
                  Icons.menu,
                ),
              ),
              Tab(
                icon: Icon(
                  Icons.favorite_outline_rounded,
                ),
              ),
              Tab(
                icon: Icon(
                  Icons.lock_outlined,
                ),
              ),
            ],
            unselectedLabelColor: Colors.grey.withOpacity(0.7),
            labelColor: Colors.black,
            indicatorColor: Colors.black,
          ),
          Expanded(
            child: TabBarView(
              controller: tabController,
              children: [
                GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: _images.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 2,
                    mainAxisSpacing: 2,
                    childAspectRatio: 0.9,
                  ),
                  itemBuilder: (context, index) {
                    return Container(
                      height: 160,
                      decoration: BoxDecoration(
                          color: Colors.black26,
                          border: Border.all(color: Colors.white70, width: .5)),
                      child: FittedBox(
                        child: CachedNetworkImage(
                          fit: BoxFit.fill,
                          imageUrl: _images[index],
                          placeholder: (context, url) => Padding(
                            padding: const EdgeInsets.all(35.0),
                            child: CircularProgressIndicator(),
                          ),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                        fit: BoxFit.fill,
                      ),
                    );
                  },
                ),
                Center(
                  child: Text("You will find liked posts here",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ),
                Center(
                  child: Text("You will find your private posts here",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
