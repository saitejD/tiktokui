import 'package:flutter/material.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        title: Container(
          height: 38,
          decoration: BoxDecoration(
            color: Colors.grey.shade200,
            // border: Border.all(color: Colors.grey.shade400, width: 0.5),
            borderRadius: BorderRadius.circular(5),
          ),
          child: TextField(
            cursorColor: Colors.black.withOpacity(0.5),
            decoration: InputDecoration(
              border: InputBorder.none,
              prefixIcon:
                  Icon(Icons.search, color: Colors.black.withOpacity(0.5)),
              hintText: 'Search',
            ),
          ),
        ),
        actions: [
          // Container(width: 10),
          Icon(Icons.qr_code, color: Colors.black),
          Container(width: 10),
        ],
      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Center(
            child: Text("Search for people or videoes",
                style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: 18,
                  color: Colors.black,
                )),
          ),
          // child: Column(
          //   children: [
          //     Container(
          //       decoration: BoxDecoration(
          //         // color: Colors.white,
          //         border: Border(bottom: BorderSide(color: Colors.black12)),
          //       ),
          //       padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
          //       child: Row(
          //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //         children: [
          //           Expanded(
          //             child: Container(
          //               height: 40,
          //               decoration: BoxDecoration(
          //                 color: Colors.grey.shade300,
          //                 // border: Border.all(color: Colors.grey.shade400, width: 0.5),
          //                 borderRadius: BorderRadius.circular(5),
          //               ),
          //               child: TextField(
          //                 cursorColor: Colors.black.withOpacity(0.5),
          //                 decoration: InputDecoration(
          //                   border: InputBorder.none,
          //                   prefixIcon: Icon(Icons.search,
          //                       color: Colors.black.withOpacity(0.5)),
          //                   hintText: 'Search',
          //                 ),
          //               ),
          //             ),
          //           ),
          //           SizedBox(width: 10),
          //           Icon(Icons.qr_code),
          //         ],
          //       ),
          //     ),
          //   ],
          // ),
        ),
      ),
    );
  }
}
